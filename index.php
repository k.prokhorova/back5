<?php
header('Content-Type: text/html; charset=UTF-8');
$user = 'u23768';
$password = '4433413';
$db = new PDO('mysql:host=localhost;dbname=u23768', $user, $password, array(PDO::ATTR_PERSISTENT => true));
$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    $messages = array();
    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        $messages[] = '<div>Спасибо, результаты сохранены.</div>';
        if (!empty($_COOKIE['pass'])) {
            $messages[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
                strip_tags($_COOKIE['login']),
                strip_tags($_COOKIE['pass']));
        }
    }
    $errors = array();
    $errors_flag = FALSE;
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['birth_date'] = !empty($_COOKIE['birth_date_error']);
    $errors['gender'] = !empty($_COOKIE['gender_error']);
    $errors['limb'] = !empty($_COOKIE['limb_error']);
    $errors['ability'] = !empty($_COOKIE['ability_error']);
    $errors['biography'] = !empty($_COOKIE['biography_error']);
    $errors['checkbox'] = !empty($_COOKIE['checkbox_error']);
    if ($errors['fio']) {
        setcookie('fio_error', '', 100000);
        $messages[] = '<div>Поле имя не заполнено или введены недопустимые символы.</div>';
        $errors_flag = TRUE;
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div>Поле почта не заполнено или введены недопустимые символы.</div>';
        $errors_flag = TRUE;
    }
    if ($errors['birth_date']) {
        setcookie('birth_date_error', '', 100000);
        $messages[] = '<div>Заполните дату рождения.</div>';
        $errors_flag = TRUE;
    }
    if ($errors['gender']) {
        setcookie('gender_error', '', 100000);
        $messages[] = '<div>Выберите пол.</div>';
        $errors_flag = TRUE;
    }
    if ($errors['limb']) {
        setcookie('limb_error', '', 100000);
        $messages[] = '<div>Выберие число конечностей.</div>';
        $errors_flag = TRUE;
    }
    if ($errors['ability']) {
        setcookie('ability_error', '', 100000);
        $messages[] = '<div>Выберие сверхспособности.</div>';
        $errors_flag = TRUE;
    }
    if ($errors['biography']) {
        setcookie('biography_error', '', 100000);
        $messages[] = '<div>Заполните биографию.</div>';
        $errors_flag = TRUE;
    }
    if ($errors['checkbox']) {
        setcookie('checkbox_error', '', 100000);
        $messages[] = '<div>Подтвердите, что ознакомились с контрактом.</div>';
        $errors_flag = TRUE;
    }
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : strip_tags($_COOKIE['fio_value']);
    $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
    $values['birth_date'] = empty($_COOKIE['birth_date_value']) ? '' : strip_tags($_COOKIE['birth_date_value']);
    $values['gender'] = empty($_COOKIE['gender_value']) ? '' : strip_tags($_COOKIE['gender_value']);
    $values['limb'] = empty($_COOKIE['limb_value']) ? '' : strip_tags($_COOKIE['limb_value']);
    $values['ability'] = empty($_COOKIE['ability_value']) ? '' : unserialize($_COOKIE['ability_value']);
    $values['biography'] = empty($_COOKIE['biography_value']) ? '' : strip_tags($_COOKIE['biography_value']);
    $values['checkbox'] = empty($_COOKIE['checkbox_value']) ? '' : strip_tags($_COOKIE['checkbox_value']);
    if (!$errors_flag && !empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
        $login = $_SESSION['login'];
        $result = $db->prepare("SELECT id, name, email, year, gender, limb, bio, checkbox FROM form WHERE login = '$login'");
        $data = $result->fetch(PDO::FETCH_ASSOC);
        $result->execute();
        $values['id'] = strip_tags($data['id']);
        $values['fio'] = strip_tags($data['name']);
        $values['email'] = strip_tags($data['email']);
        $values['birth_date'] = strip_tags($data['year']);
        $values['gender'] = strip_tags($data['gender']);
        $values['limbs'] = strip_tags($data['limb']);
        $values['bio'] = strip_tags($data['bio']);
        $values['checkbox'] = strip_tags($data['checkbox']);
        $values['login'] = $login;
        $id_from_db = $values['id'];
        $result = $db->prepare("SELECT ability_id FROM ability WHERE form_id = '$id_from_db'");
        $data = $result->fetch(PDO::FETCH_ASSOC);
        $result->execute();
        $values['ability'] = serialize($data['ability_id']);
    }
    include('form.php');
} else {
    $errors = FALSE;
    if (!preg_match("/^[A-Za-z]+$/", $_POST['fio']) || empty($_POST['fio'])) {
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    if (!preg_match("/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/", $_POST['email']) || empty($_POST['email'])) {
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['birth_date']) || !is_numeric($_POST['birth_date'])) {
        setcookie('birth_date_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('birth_date_value', $_POST['birth_date'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['gender'])) {
        setcookie('gender_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['limb']) || !is_numeric($_POST['limb'])) {
        setcookie('limb_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('limb_value', $_POST['limb'], time() + 30 * 24 * 60 * 60);
    }
    $myselect = $_POST['select'];
    $flag = 0;
    if (empty($myselect)) {
        setcookie('ability_error', '1', time() + 24 * 60 * 60);
        $flag = 1;
        $errors = TRUE;
    }
    if (!empty($myselect)) {
        foreach ($myselect as $ability) {
            if (!is_numeric($ability)) {
                setcookie('ability_error', '2', time() + 24 * 60 * 60);
                $flag = 1;
                $errors = TRUE;
                break;
            }
        }
    }
    if ($flag == 0) {
        setcookie('ability_value', serialize($myselect), time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['biography'])) {
        setcookie('biography_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['checkbox']) || !is_numeric($_POST['checkbox'])) {
        setcookie('checkbox_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    } else {
        setcookie('checkbox_error', '', 100000);
    }
    if ($errors) {
        header('Location: index.php');
        exit();
    } else {
        setcookie('fio_error', '', 100000);
        setcookie('email_error', '', 100000);
        setcookie('birth_date_error', '', 100000);
        setcookie('gender_error', '', 100000);
        setcookie('limb_error', '', 100000);
        setcookie('ability_error', '', 100000);
        setcookie('biography_error', '', 100000);
        setcookie('checkbox_error', '', 100000);
    }
    if (!empty($_COOKIE[session_name()]) &&
        session_start() && !empty($_SESSION['login'])) {
        $name = $_POST['fio'];
        $email = $_POST['email'];
        $birth_date = $_POST['birth_date'];
        $gender = $_POST['gender'];
        $limb = $_POST['limb'];
        $bio = $_POST['biography'];
        $checkbox = $_POST['checkbox'];
        $login = $_SESSION['login'];
        $id = $_SESSION['uid'];
        $stmt = $db->prepare("UPDATE form 
        SET name = '$name', 
            email = '$email', 
            year = '$birth_date', 
            gender ='$gender', 
            limb = '$limb', 
            bio = '$bio' 
        WHERE login='$login'");
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $stmt->execute();

        $stmt = $db->prepare("SELECT id from form WHERE login='$login'");
        $stmt->execute();
        $data_id = $stmt->fetch(PDO::FETCH_ASSOC);
        $id = $data_id['id'];
        $stmt = $db->prepare("DELETE from ability WHERE id='$id' ");
        $stmt->execute();
        $myselect = $_POST['select'];
        $abilities = array('1', '2', '3');
        foreach ($myselect as $ability) {
            if (in_array($ability, $abilities)) {
                $stmt = $db->prepare("INSERT INTO ability (form_id, ability_id) VALUES (:id, :ability)");
                $stmt->execute(array('id' => $id, 'ability' => $ability));
            }
        }

    } else {
        $login = uniqid();
        $pass = substr(strval(rand()), 0, 8);
        setcookie('login', $login);
        setcookie('pass', $pass);
        $password = password_hash($pass, PASSWORD_DEFAULT);
        $stmt = $db->prepare("INSERT INTO form (name, email, year, gender, limb, bio, checkbox, login, pass) 
            VALUES (:fio, :email, :birth_date, :gender, :limb, :bio, :checkbox, :login, :pass)");
        $stmt->execute(array(
            'fio' => $_POST['fio'],
            'email' => $_POST['email'],
            'birth_date' => $_POST['birth_date'],
            'gender' => $_POST['gender'],
            'limb' => $_POST['limb'],
            'bio' => $_POST['biography'],
            'checkbox' => $_POST['checkbox'],
            'login' => $login,
            'pass' => $password));
        $form_id = $db->lastInsertId();
        $myselect = $_POST['select'];
        if (!empty($myselect)) {
            foreach ($myselect as $ability) {
                if (!is_numeric($ability)) {
                    continue;
                }
                $stmt = $db->prepare("INSERT INTO ability (form_id, ability_id) VALUES (:form_id, :ability_id)");
                $stmt->execute(array(
                    'form_id' => $form_id,
                    'ability_id' => $ability
                ));
            }
        }
    }
    setcookie('save', '1');
    header('Location: ./');
}
