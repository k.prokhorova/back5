<head>
    <meta charset="utf-8">
    <title>5</title>
    <link rel="stylesheet" media="all" href="style.css">
    <style>
        .error {
            border: 2px solid red;
        }
    </style>
</head>

<body>
<?php
if (!empty($messages)) {
    print('<div id="messages">');
    foreach ($messages as $message) {
        print($message);
    }
    print('</div>');
}

if (!empty($values['login'])) {
    print('<a href="login.php">Выход</a>');
}
?>

<form action="" accept-charset="UTF-8" method="POST">
    <div class="form">
        <div class="border">
            <h2>Регистрация</h2>

            <label>Введите имя</label><br>
            <label><input type="text" name="fio" <?php if ($errors['fio']) {
                    print 'class="error"';
                } ?> value="<?php print $values['fio']; ?>"></label><br>

            <label>Введите почту</label><br>
            <label><input type="email" name="email" <?php if ($errors['email']) {
                    print 'class="error"';
                } ?> value="<?php print $values['email']; ?>"></label><br>

            <label>Введите свой год рождения</label><br>
            <select name="birth_date">
                <?php for ($i = 1900; $i < 2022; $i++) { ?>
                    <option value="<?php print($i); ?>"><?php print($i); ?></option><?php } ?>
            </select>
            <br>

            <label>Выберите пол:</label><br>
            <label <?php if ($errors['gender']) {
                print 'class = "error"';
            } ?>><input type="radio" name="gender" value="man">Мужской</label>
            <label <?php if ($errors['gender']) {
                print 'class = "error"';
            } ?>><input type="radio" name="gender" value="woman">Женский</label>
            <br>

            <label>Количество конечностей:</label><br>
            <label <?php if ($errors['limb']) {
                print 'class = "error"';
            } ?>><input type="radio" name="limb" value="1">1</label>
            <label <?php if ($errors['limb']) {
                print 'class = "error"';
            } ?>><input type="radio" name="limb" value="2">2</label>
            <label <?php if ($errors['limb']) {
                print 'class = "error"';
            } ?>><input type="radio" name="limb" value="3">3</label>
            <label <?php if ($errors['limb']) {
                print 'class = "error"';
            } ?>><input type="radio" name="limb" value="4">4</label>
            <br>

            <label>Ваши сверхспособности:</label><br>
            <select name="select[]" multiple="multiple" <?php if ($errors['limb']) {
                print 'class = "error"';
            } ?>>
                <option name="immortality" value="1">Бессмертие</option>
                <option name="invisibility" value="2">Невидимость</option>
                <option name="levitation" value="3">Левитация</option>
            </select>
            <br>

            <label>Биография:</label><br>
            <textarea name="biography" <?php if ($errors['biography']) {
                print 'class="error"';
            } ?>><?php print $values['biography']; ?></textarea>
            <br>

            <label>Нажмите, если ознакомились с контрактом:</label><br>
            <label <?php if ($errors['checkbox']) {
                print 'class = "error"';
            } ?>><input type="checkbox" name="checkbox" value="1">С контрактом ознакомлен</label>
            <br>

            <input type="submit" value="Отправить">
        </div>
    </div>
</body>

